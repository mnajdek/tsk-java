package main;

public class RotationVelocityOutOfRange extends Exception {

	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Rotation velocity is out of Range!!";
		
	}
}
