package main;

/**
 * Class Whirpool defines an object with:
 * - maxProgram value equal 25.
 * - BrandName value equal BrandName.WHIRPOOL 
 * - other values are inherited.
 * 
 * @param int programNumber
 * @param double temperatureValue
 * @param int rotationVelocity
 * @author MNAJDEK
 * 
 * Class Whirpool inherits from WashingMachine Class.
 */
public class Whirpool extends WashingMachine{

	public Whirpool(int programNumber, double temperatureValue, int rotationVelocity) {
		super();
		super.setBrandName(BrandName.WHIRPOOL);
		super.maxProgram = 25;
		super.setProgramNumber(programNumber);
		super.setTemperatureValue(temperatureValue);
		super.setRotationVelocity(rotationVelocity);
	}

}
