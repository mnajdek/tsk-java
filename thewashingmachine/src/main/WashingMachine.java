package main;

public abstract class WashingMachine {

	private int programNumber = 1;
	protected double temperatureValue = 0.0;
	private BrandName brandName;
	private int rotationVelocity = 0;
	
	//WaschingMachines parameters
	protected int maxProgram = 20;
	private final double maxTemp = 90.0; // final?
	private final int maxRotation = 1000; // final?
	protected double tempStep = 0.5;
	protected double tempSetup = 2;
	
	//Getters & Setters
	public int getProgramNumber() {
		return programNumber;
	}
	
	//programNumber
	public void setProgramNumber(int programNumber){		
		if(programNumber <= maxProgram && programNumber >= 1) {
			this.programNumber = programNumber;
		} else {
			try {
				throw new ProgramOutOfRange();
			} catch (ProgramOutOfRange e) {
				e.printStackTrace();
			}
		}
	}
	
	//temperatureValue
	public double getTemperatureValue() {
		return temperatureValue;
	}
	
	/**
	 * This method is setting and validating a new temperature value.
	 * The value has to be between 0 and 90 Celsius degrees including.
	 * @param temperatureValue
	 * @throws TemperatureOutOfRange
	 */
	public void setTemperatureValue(double temperatureValue){
		if(temperatureValue <=maxTemp && temperatureValue >=0.0) {
			this.temperatureValue = Math.round(temperatureValue * tempSetup) / tempSetup;	
		} else {
			try {
				throw new TemperatureOutOfRange();
			} catch (TemperatureOutOfRange e) {
				e.printStackTrace();
			}
		}
	}
	
	//BrandName
	public BrandName getBrandName() {
		return brandName;
	}
	public void setBrandName(BrandName brandName) {
		this.brandName = brandName;
	}
	
	//rotationVelocity
	public int getRotationVelocity() {
		return rotationVelocity;
	}
	/**
	 * This method is setting and validating a new Rotation value.
	 * The value has to be between 0 and 1000 RPM including.
	 * @param rotationVelocity
	 * @throws RotationVelocityOutOfRange
	 */
	public void setRotationVelocity(int rotationVelocity) {
		if(rotationVelocity <= maxRotation && rotationVelocity >=0) {
				this.rotationVelocity = (int)Math.round((double)rotationVelocity / 100) * 100;					
		} else {
			try {
				throw new RotationVelocityOutOfRange();
			} catch (RotationVelocityOutOfRange e) {
				e.printStackTrace();
			}
		}
	}

	//maxProgram
	public int getMaxProgram() {
		return maxProgram;
	}
	
	//MaxTemp
	public double getMaxTemp() {
		return maxTemp;
	}	

	//Constructor
	public WashingMachine() {
		this.programNumber = 1;
		this.temperatureValue = 0;
		this.rotationVelocity = 0;
	}
	
	
	
	//Methods section
	
	@Override
	public String toString() {
		return "WaschingMachine [brandName=" + brandName + "]" + System.lineSeparator();
	}

	//nextProgram Methods
	public void nextProgram () {
		if(this.programNumber >= 1 && this.programNumber < maxProgram) {
			this.programNumber++;
		} else {
			this.programNumber = 1;
		}
	}
	
	public void previousProgram () {
		if(this.programNumber > 1 && this.programNumber <= maxProgram) {
			this.programNumber--;
		} else {
			this.programNumber = maxProgram;
		}
	}
	
	/**
	 * This method increases temperatureValue by 1 Celsius degree.
	 * Method made for instances of WashingMachine class.
	 * 
	 * @throws TemperatureOutOfRange
	 */
	public void tempUp () throws TemperatureOutOfRange {
		if(this.temperatureValue >= 0 && this.temperatureValue < maxTemp) {
			this.temperatureValue += tempStep;
			System.out.println("Current temp: " + this.temperatureValue + "\u00B0C" );
		} else {
			throw new TemperatureOutOfRange();
		}
	}
	/**
	 * This method decreases temperatureValue by 1 Celsius degree.
	 * Method made for instances of WashingMachine class.
	 * 
	 * @throws TemperatureOutOfRange
	 */
	public void tempDown () {
		if(this.temperatureValue > 0 && this.temperatureValue <= maxTemp) {
			this.temperatureValue -= tempStep;
			System.out.println("Current temp: " + this.temperatureValue + "\u00B0C");
		} else {
			
			try {
				throw new TemperatureOutOfRange();
			} catch (TemperatureOutOfRange e) {
				e.printStackTrace();
			}
		}
	}
	
	// rotationVelocity Methods
	public void rotationVelocityUp () {
		if(this.rotationVelocity >= 0 && this.rotationVelocity < maxRotation) {
			this.rotationVelocity += 100;
		} else {
			this.rotationVelocity = 0;
		}
	}
	
	public void rotationVelocityDown () {
		if(this.rotationVelocity > 0 && this.rotationVelocity <= maxRotation) {
			this.rotationVelocity -= 100;
		} else {
			this.rotationVelocity = maxRotation;
		}
	}
	
	/**
	 * This method generates a report of the Washing machine status.
	 * 
	 * @throws TemperatureOutOfRange
	 */
	public void showStatus() {
		
		System.out.println("------Status Start------");
		System.out.println("Object: " + this);		
		System.out.println("Active program: " + this.programNumber);
		System.out.println("Max program: " + this.maxProgram);
		System.out.println("Current temp: " + this.temperatureValue + "\u00B0C");
		System.out.println("Temp step: " + this.tempStep + "\u00B0C");			
		System.out.println("Brand: " + this.brandName);	
		System.out.println("Current rotations: " + this.rotationVelocity + " RPS");	
		System.out.println("Max rotations: " + this.maxRotation + " RPS");	
		System.out.println("------Status End--------" + System.lineSeparator());

	}
}