/**
* The program generates objects that are Washing machines classes.
* 
*
* @author  Michal Najdek
* @version 1.0
* @since   2018-10-24
*/


package main;

import java.util.ArrayList;
import java.util.List;

public class Main {

	public static void main(String[] args) {
		
		Beko newBeko = new Beko(5, 5, -5);
		Whirpool newWhirpool = new Whirpool(5, 5, 5);
		Amica newAmica = new Amica(5,5,5);
		
		List<WashingMachine> listOfWaschMachines = new ArrayList<WashingMachine>();
		
		listOfWaschMachines.add(newBeko);
		listOfWaschMachines.add(newWhirpool);
		listOfWaschMachines.add(newAmica);
			
		newBeko.setTemperatureValue(90);
		newBeko.setProgramNumber(5);

		
		
		int newVal = (int) newBeko.getTemperatureValue();
		
		for (int i = 0; i <= newVal; i++) {
			System.out.println(newBeko.getTemperatureValue());
			newBeko.tempDown();
		}
		
	}
	
	public static void sortList(List<WashingMachine> list) {
		System.out.println("Method sort initialized. Print result before sorting:");
		list.stream()
				.forEach(System.out::print);
		System.out.println("Result after sorting:");
		list.stream()
				.sorted((e2,e1) -> e2.getBrandName()
						.name()
						.compareTo(e1.getBrandName().name()))
				.forEach(System.out::print);
	}
	
	
}
