package main;

public class Amica extends WashingMachine{

	/**
	 * Class Amica defines an object with:
	 * - maxProgram value equal 25.
	 * - BrandName value equal BrandName.WHIRPOOL 
	 * - other values are inherited.
	 * 
	 * @param int programNumber
	 * @param double temperatureValue
	 * @param int rotationVelocity
	 * @author MNAJDEK
	 * 
	 * Class Amica inherits from WashingMachine Class.
	 */	
	public Amica(int programNumber, double temperatureValue, int rotationVelocity) {
		super();
		super.setBrandName(BrandName.AMICA);
		super.setProgramNumber(programNumber);
		super.setTemperatureValue(temperatureValue);
		super.setRotationVelocity(rotationVelocity);
	}
	
}
