package main;

public class TemperatureOutOfRange extends Exception{
	
	private static final long serialVersionUID = 1L;

	@Override
	public String getMessage() {
		return "Temperature is out of Range!!";
	}

}
