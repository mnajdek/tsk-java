package main;

/**
 * Class Beko defines an object with:
 * - temperature step 1 Celsius degree.
 * - tempSetup value equal 1.
 * - BrandName value equal BrandName.BEKO 
 *- other values are inherited.
 *
 * @param int programNumber
 * @param double temperatureValue
 * @param int rotationVelocity
 * @author MNAJDEK
 * 
 * Class Beko inherits from WashingMachine Class.
 */
public class Beko extends WashingMachine{

	public Beko(int programNumber, double temperatureValue, int rotationVelocity) {
		super();
		super.setBrandName(BrandName.BEKO);
		super.tempStep = 1.0;
		super.tempSetup = 1;
		super.setProgramNumber(programNumber);
		super.setTemperatureValue(temperatureValue);
		super.setRotationVelocity(rotationVelocity);
	}
}
